// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: Apache-2.0
#include "pqTracingStarter.h"
#include "vtkLogTracer.h"

//-----------------------------------------------------------------------------
pqTracingStarter::pqTracingStarter(QObject* p)
  : QObject(p)
{
}

//-----------------------------------------------------------------------------
pqTracingStarter::~pqTracingStarter() = default;

//-----------------------------------------------------------------------------
void pqTracingStarter::onStartup()
{
  this->Manager->Start();
}

//-----------------------------------------------------------------------------
void pqTracingStarter::onShutdown()
{
  this->Manager->Stop();
}

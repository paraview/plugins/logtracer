set(interfaces)
set(sources
  pqTracingStarter.cxx
  pqTracingStarter.h)

if (PARAVIEW_USE_QT)
  paraview_plugin_add_auto_start(
    CLASS_NAME pqTracingStarter
    STARTUP onStartup
    SHUTDOWN onShutdown
    INTERFACES autostart_interface
    SOURCES autostart_sources)
  list(APPEND interfaces ${autostart_interface})
  list(APPEND sources ${autostart_sources})

  paraview_add_plugin(PVTracing
    VERSION       "1.0"
    REQUIRED_ON_CLIENT

    UI_INTERFACES ${interfaces}
    SOURCES       ${sources}
    MODULES       PVTracing::vtkLogTracer
    MODULE_FILES  "${CMAKE_CURRENT_SOURCE_DIR}/Sources/vtk.module")
else()
  message(WARNING "Ignoring PVTracing plugin (missing PARAVIEW_USE_QT=ON)")
endif()

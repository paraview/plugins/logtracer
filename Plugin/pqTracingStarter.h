// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: Apache-2.0
#ifndef pqTracingStarter_h
#define pqTracingStarter_h

#include <QObject>

#include "vtkNew.h"

class vtkLogTracer;

class pqTracingStarter : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  pqTracingStarter(QObject* p = nullptr);
  virtual ~pqTracingStarter();

  void onShutdown();

  void onStartup();

private:
  Q_DISABLE_COPY(pqTracingStarter)

  vtkNew<vtkLogTracer> Manager;
};

#endif

// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: Apache-2.0
#include "vtkLogTracer.h"

#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqPipelineFilter.h"
#include "pqPipelineSource.h"
#include "pqServer.h"
#include "pqServerManagerModel.h"
#include "pqServerResource.h"

#include "vtkDelimitedTextReader.h"
#include "vtkDelimitedTextWriter.h"
#include "vtkIdTypeArray.h"
#include "vtkMergeTables.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPVLogInformation.h"
#include "vtkPVServerInformation.h"
#include "vtkPVStringFormatter.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxy.h"
#include "vtkSMSession.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkStringArray.h"
#include "vtkTable.h"
#include "vtkVariantArray.h"

#include "vtksys/FStream.hxx"
#include "vtksys/SystemTools.hxx"

#include <ctime>
#include <locale>
#include <random>
#include <sstream>

namespace details
{
std::string TimeToString(const std::time_t& t, const char* format = "%F %T")
{
  static char buffer[50];
  const std::size_t size = std::strftime(buffer, sizeof(buffer), format, std::localtime(&t));
  return std::string(buffer, buffer + size);
}

inline bool EndsWith(const std::string& value, const std::string& ending)
{
  if (ending.size() > value.size())
  {
    return false;
  }
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

std::string BeautifyLog(const std::string& line, std::size_t begin)
{
  std::string msg;
  std::size_t pos = line.find('{', begin);
  if (pos != std::string::npos)
  {
    msg = std::string("Start -");
    pos++;
  }
  else
  {
    pos = line.find('}', begin);
    if (pos != std::string::npos)
    {
      msg = std::string("End -");
      pos++;
    }
    else
    {
      pos = begin;
    }
  }
  return msg.append(std::string(line.begin() + pos, line.end()));
}
};

//----------------------------------------------------------------------------
class vtkLogTracer::vtkInternal
{
public:
  vtkInternal()
  {
    this->Username = vtkPVStringFormatter::Format("{ENV_username}");

    // Initialize session ID.
    // This is only a 64 bit randomness number while a real GUID is 128 bit random.
    std::random_device rd;
    std::mt19937_64 gen(rd());
    this->SessionId = static_cast<uint_fast64_t>(gen());

    this->StartupTime = std::time(nullptr);

    std::string filePath;
    if (vtksys::SystemTools::GetEnv(vtkLogTracer::LogPathEnvVariable, filePath))
    {
      if (vtksys::SystemTools::FileExists(filePath) &&
        vtksys::SystemTools::FileIsDirectory(filePath))
      {
        vtkGenericWarningMacro(
          "Cannot write trace at " << this->FilePath << ", file is a directory.");
      }
      else
      {
        this->FilePath = filePath;
      }
    }

    const std::string& sessStr = std::to_string(this->GetSessionId());
    this->SessionFilePath = filePath + "-" + sessStr;

    if (vtksys::SystemTools::FileExists(this->SessionFilePath) &&
      vtksys::SystemTools::FileIsDirectory(this->SessionFilePath))
    {
      vtkGenericWarningMacro(
        "Cannot write trace at " << this->SessionFilePath << ", file is a directory.");
    }
  }

  ~vtkInternal()
  {
    if (vtksys::SystemTools::FileExists(this->SessionFilePath.c_str()))
    {
      vtksys::SystemTools::RemoveFile(this->SessionFilePath.c_str());
    }
  }

  /**
   * Initialize the logger according to the current state of ParaView.
   * This should be safe to be called multiple times as this should be called each time
   * the ParaView client connects to a different ParaView server.
   */
  void InitializeLogger()
  {
    // Create a log recorder on the server side
    if (pqActiveObjects::instance().activeServer())
    {
      this->InitializeLoggerInternal();
    }
    else
    {
      QObject::connect(pqApplicationCore::instance(), &pqApplicationCore::clientEnvironmentDone,
        [this]() { this->InitializeLoggerInternal(); });
    }
  }

  void ClearRemoteLogs()
  {
    if (this->LogRecorderProxy)
    {
      this->LogRecorderProxy->InvokeCommand("ClearLogs");
    }
  }

  /**
   * Replace local logs with the logs from the server.
   */
  void Refresh()
  {
    if (this->LogRecorderProxy)
    {
      vtkNew<vtkPVLogInformation> logInformation;
      logInformation->SetRank(this->Rank);
      this->LogRecorderProxy->GatherInformation(logInformation);
      this->CurrentLogs = logInformation->GetLogs();
    }
  }

  /**
   * Return a tuple [Date, MsgPos].
   *
   * - Date: date of the execution of the process.
   * - MsgPos: position in the input string corresponding to where the meta data ends
   * and the actual message begin.
   *
   * A typical message from the PV logger is :
   * (   36.540s) [paraview    ]  vtkSMProxy.cxx:854                9| { vtkSMProxy
   * (0x56522f854ff0)[misc, LogRecorder]: gather information vtkPVLogInformation ( __TIME__s)
   * [__PROCESS__ ]  __FILENAME__:__LINE__  __LOGLEVEL__| __MSG__
   *
   *  - __TIME__ : time ellapsed since PV startup when this log was recorded
   *  - __PROCESS__ : the name of the process where thos log was created (usually `paraview` or
   * `pvserver`)
   *  - __FILENAME__ : filename of the source code
   *  - __LINE__ : exact line of the log in __FILENAME__
   *  - __LOGLEVEL__ : E [0,9], log level. The greater the more log we get.
   *  - __MSG__ : the actual log message. This message could also be further parsed to get the class
   * name, the `this` pointer, etc.
   */
  std::tuple<std::string, std::size_t> ParseLogLine(const std::string& line)
  {
    std::time_t dateSeconds = this->StartupTime;
    std::size_t pos = 0;

    // Extract time
    std::size_t open = line.find('(', pos);
    std::size_t close = line.find(')', pos);
    if (open != std::string::npos && close != std::string::npos)
    {
      pos = close + 1;
      std::string timeStr = line.substr(open + 1, close - open);
      timeStr.erase(std::remove_if(timeStr.begin(), timeStr.end(),
                      [](char& c) { return std::isspace<char>(c, std::locale::classic()); }),
        timeStr.end());

      // TODO: this only works if we consider that the plugin is initialized at the same time that
      // the loguru logger on ParaView, which may not be the case if the plugin is not loaded at PV
      // startup
      dateSeconds += static_cast<std::time_t>(std::roundf(std::stof(timeStr)));
    }

    // Find beginning of the actual log
    close = line.find('|', pos);
    if (close != std::string::npos)
    {
      pos = close + 1;
    }

    return { details::TimeToString(dateSeconds), pos };
  }

  void ReadLogFile(vtkTable* file)
  {
    if (!vtksys::SystemTools::FileExists(this->FilePath.c_str()))
    {
      return;
    }

    vtkNew<vtkDelimitedTextReader> reader;
    reader->SetFileName(this->GetFilePath().c_str());
    reader->SetHaveHeaders(true);
    reader->SetFieldDelimiterCharacters(";");
    reader->DetectNumericColumnsOff();
    reader->Update();
    vtkTable* previousFile = reader->GetOutput();

    // Check that the file we just read is correct
    if (previousFile != nullptr && previousFile->GetNumberOfColumns() == 4 &&
      std::string("User") == previousFile->GetColumn(0)->GetName() &&
      std::string("Session") == previousFile->GetColumn(1)->GetName() &&
      std::string("Date") == previousFile->GetColumn(2)->GetName() &&
      std::string("Message") == previousFile->GetColumn(3)->GetName())
    {
      file->ShallowCopy(previousFile);
    }
    else
    {
      vtkGenericWarningMacro("Tracing plugin: log file '"
        << this->GetFilePath() << "' cannot be parsed, content will be overriden.");
    }
  }

  std::string GetLog() const { return this->CurrentLogs; }
  std::string GetUsername() const { return this->Username; }
  uint_fast64_t GetSessionId() const { return this->SessionId; }
  std::string GetSessionFilePath() const { return this->SessionFilePath; }
  std::string GetFilePath() const { return this->FilePath; }

private:
  void InitializeLoggerInternal()
  {
    pqServer* server = pqActiveObjects::instance().activeServer();
    vtkSmartPointer<vtkSMSessionProxyManager> pxm = server->proxyManager();
    this->LogRecorderProxy.TakeReference(pxm->NewProxy("misc", "LogRecorder"));

    this->LogRecorderProxy->SetLocation(vtkSMSession::CLIENT_AND_SERVERS);
    vtkSMPropertyHelper(this->LogRecorderProxy, "RankEnabled").Set(this->Rank);
    vtkSMPropertyHelper(this->LogRecorderProxy, "Verbosity").Set(VERBOSITY);
    vtkSMPropertyHelper(this->LogRecorderProxy, "CategoryVerbosity").Set(0, PIPELINE_CATEGORY);
    vtkSMPropertyHelper(this->LogRecorderProxy, "CategoryVerbosity").Set(1, VERBOSITY);
    this->LogRecorderProxy->UpdateVTKObjects();
  }

  uint_fast64_t SessionId = 0;
  std::string Username = "";
  std::time_t StartupTime = 0;

  int Rank = 0;
  vtkSmartPointer<vtkSMProxy> LogRecorderProxy = nullptr;
  std::string CurrentLogs = "";

  std::string SessionFilePath = "";
  std::string FilePath = "";

  constexpr static int PIPELINE_CATEGORY = 3;
  constexpr static int VERBOSITY = 9;
};

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkLogTracer);
constexpr const char* vtkLogTracer::LogPathEnvVariable;

//----------------------------------------------------------------------------
vtkLogTracer::vtkLogTracer()
  : Internal(new vtkInternal)
{
  this->Writer->SetFileName(this->Internal->GetSessionFilePath().c_str());
  this->Writer->SetFieldDelimiter(";");

  vtkNew<vtkStringArray> userColumn;
  userColumn->SetName("User");
  vtkNew<vtkStringArray> sessionColumn;
  sessionColumn->SetName("Session");
  vtkNew<vtkStringArray> msgColumn;
  msgColumn->SetName("Message");
  vtkNew<vtkStringArray> dateColumn;
  dateColumn->SetName("Date");
  this->CurrentTrace->AddColumn(userColumn);
  this->CurrentTrace->AddColumn(sessionColumn);
  this->CurrentTrace->AddColumn(dateColumn);
  this->CurrentTrace->AddColumn(msgColumn);
}

//----------------------------------------------------------------------------
vtkLogTracer::~vtkLogTracer() = default;

//----------------------------------------------------------------------------
int vtkLogTracer::Start()
{
  this->Writer->SetFileName(this->Internal->GetSessionFilePath().c_str());
  this->Internal->InitializeLogger();
  this->AddEntry("Session Starts.");
  this->WriteSessionFile();

  QObject::connect(pqApplicationCore::instance()->getServerManagerModel(),
    &pqServerManagerModel::serverAdded,
    [this](pqServer* server)
    {
      this->Internal->InitializeLogger();
      const char* uri = server->session()->GetURI();

      this->AddEntry(std::string("Connect - ParaView server: '").append(uri).append("'").c_str());
      this->WriteSessionFile();
    });

  QObject::connect(pqApplicationCore::instance()->getServerManagerModel(),
    &pqServerManagerModel::serverRemoved,
    [this](pqServer* server)
    {
      const char* uri = server->session()->GetURI();

      this->AddEntry(
        std::string("Disconnect - ParaView server: '").append(uri).append("'").c_str());
      this->WriteSessionFile();
    });

  // Fired everytime a proxy is added. We track only pipeline source/filter
  QObject::connect(pqApplicationCore::instance()->getServerManagerModel(),
    &pqServerManagerModel::proxyAdded,
    [this](pqProxy* proxy)
    {
      if (auto pqfilter = qobject_cast<pqPipelineFilter*>(proxy))
      {
        std::string name = pqfilter->getSMName().toStdString();
        this->AddEntry(std::string("Create (filter) - ").append(name).c_str());
      }
      else if (auto pqsource = qobject_cast<pqPipelineSource*>(proxy))
      {
        std::string name = pqsource->getSMName().toStdString();
        this->AddEntry(std::string("Create (source) - ").append(name).c_str());
      }
      // skip other kind of proxy, like View or Representation.

      this->FetchAndProcessLog();
      this->WriteSessionFile();
    });

  // Fired everytime a proxy is removed. We track only pipeline source/filter
  QObject::connect(pqApplicationCore::instance()->getServerManagerModel(),
    &pqServerManagerModel::proxyRemoved,
    [this](pqProxy* proxy)
    {
      if (auto pqfilter = qobject_cast<pqPipelineFilter*>(proxy))
      {
        std::string name = pqfilter->getSMName().toStdString();
        this->AddEntry(std::string("Delete (filter) - ").append(name).c_str());
      }
      else if (auto pqsource = qobject_cast<pqPipelineSource*>(proxy))
      {
        std::string name = pqsource->getSMName().toStdString();
        this->AddEntry(std::string("Delete (source) - ").append(name).c_str());
      }
      // skip other kind of proxy, like View or Representation.

      this->WriteSessionFile();
    });

  return 1;
}

//----------------------------------------------------------------------------
int vtkLogTracer::Stop()
{
  this->FetchAndProcessLog();
  this->AddEntry("Session Stop");

  vtkNew<vtkTable> previousFile;
  this->Internal->ReadLogFile(previousFile);

  vtkNew<vtkMergeTables> merger;
  merger->SetInputData(0, previousFile);
  merger->SetInputData(1, this->CurrentTrace);
  merger->Update();
  this->CurrentTrace->ShallowCopy(merger->GetOutput());

  int res = this->WriteLogFile();

  this->CurrentTrace->RemoveAllRows();

  return res;
}

//----------------------------------------------------------------------------
void vtkLogTracer::AddEntry(
  uint_fast64_t session, const char* user, const char* date, const char* msg)
{
  if (this->CurrentTrace)
  {
    // TODO: one could improve the reader to be able to write the session id
    // as an integer and not with scientific notation. Meanwhile we'll use a string
    const std::string& sessStr = std::to_string(session);

    vtkNew<vtkVariantArray> entry;
    entry->InsertNextValue(vtkVariant(user));
    entry->InsertNextValue(vtkVariant(sessStr.c_str()));
    entry->InsertNextValue(vtkVariant(date));
    entry->InsertNextValue(vtkVariant(msg));

    this->CurrentTrace->InsertNextRow(entry);
  }
}

//----------------------------------------------------------------------------
void vtkLogTracer::AddEntry(const char* msg)
{
  std::time_t currentTime = std::time(nullptr);
  std::string currentTimeStr = details::TimeToString(currentTime);
  this->AddEntry(this->Internal->GetSessionId(), this->Internal->GetUsername().c_str(),
    currentTimeStr.c_str(), msg);
}

//----------------------------------------------------------------------------
void vtkLogTracer::FetchAndProcessLog()
{
  // TODO: we may want to do this conditionally to some config.
  // For now, return and do nothing.
  return;

  // // Fetch information from the proxy
  // this->Internal->Refresh();
  // this->Internal->ClearRemoteLogs();

  // if (this->CurrentTrace)
  // {
  //   // TODO: make use of SMP to process this file (the string can be split into N equal parts and
  //   // then processed parallely if the order is kept) OR put that processing in another thread
  //   (is
  //   // it possible to do both ? ie SMP outside the main thread)
  //   std::istringstream stream(this->Internal->GetLog());
  //   for (std::string line; std::getline(stream, line);)
  //   {
  //     // Extract filter execution details
  //     if (details::EndsWith(line, "execute"))
  //     {
  //       const auto& parsed = this->Internal->ParseLogLine(line);
  //       const char* date = std::get<0>(parsed).c_str();

  //       this->AddEntry(
  //         this->Internal->GetSessionId(), this->Internal->GetUsername().c_str(), date,
  //         details::BeautifyLog(line, std::get<1>(parsed)).c_str());
  //     }
  //   }
  // }
}

//----------------------------------------------------------------------------
int vtkLogTracer::WriteSessionFile()
{
  this->Writer->SetFileName(this->Internal->GetSessionFilePath().c_str());
  this->Writer->SetInputData(this->CurrentTrace);
  return this->Writer->Write();
}

//----------------------------------------------------------------------------
int vtkLogTracer::WriteLogFile()
{
  if (this->Internal->GetFilePath().empty())
  {
    return EXIT_FAILURE;
  }

  this->Writer->SetFileName(this->Internal->GetFilePath().c_str());
  this->Writer->SetInputData(this->CurrentTrace);
  return this->Writer->Write();
}

//----------------------------------------------------------------------------
void vtkLogTracer::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);

  const char* filename = this->Writer->GetFileName();
  os << indent << vtkLogTracer::LogPathEnvVariable
     << " (env) : " << (filename ? filename : "<nullptr>") << std::endl;
}

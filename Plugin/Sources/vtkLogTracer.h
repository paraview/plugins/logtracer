// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: Apache-2.0
#ifndef vtkLogTracer_h
#define vtkLogTracer_h

#include "vtkObject.h"

#include "vtkLogTracerModule.h"
#include "vtkNew.h"

#include <memory> // for std::unique_ptr

class vtkDelimitedTextWriter;
class vtkTable;

/**
 * @class vtkLogTracer
 * @brief Handles the internal of the state and read and write it
 * from/to the disk.
 *
 * @todo maybe using a vtkDelimitedTextReader and a vtkDelimitedTextWriter
 * is overkill in our case and we could just handle that with a FILE*, which will
 * have less read/write access. Or improve the vtkDelimitedTextWriter with a
 * boolean `Append` property.
 */
class VTKLOGTRACER_EXPORT vtkLogTracer : public vtkObject
{
public:
  static vtkLogTracer* New();
  vtkTypeMacro(vtkLogTracer, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Intialize a temporary log file.
   * Create connections to the current session.
   */
  int Start();

  /**
   * Save current trace to the global log file.
   * Clear internal objects, and remove session file.
   *
   * Return 0 if failing to write the global file.
   */
  int Stop();

  /**
   * Fetch raw logs from the server and filter them to produce the expected output
   */
  void FetchAndProcessLog();

  ///@{
  /**
   * Add an entry in the current trace that register the session id, the username, the date
   * and the actual log message.
   *
   * If only the message is specified, let the internal state of the logger determine other
   * information.
   */
  void AddEntry(const char* msg);
  void AddEntry(uint_fast64_t session, const char* user, const char* msg, const char* date);
  ///@}

  ///@{
  /**
   * Overwrite pointed file with the content of CurrentTrace
   * Return 0 if it couldn't write the file.
   *
   *  @todo: improve with the Append option of the vtkDelimitedTextWriter (when feature exists)
   */
  int WriteSessionFile();
  int WriteLogFile();
  ///@}

protected:
  vtkLogTracer();
  virtual ~vtkLogTracer();

  vtkNew<vtkTable> CurrentTrace;
  vtkNew<vtkDelimitedTextWriter> Writer;

  constexpr static const char* LogPathEnvVariable = "PARAVIEW_STAT_FILE";

private:
  vtkLogTracer(const vtkLogTracer&) = delete;
  void operator=(const vtkLogTracer&) = delete;

  class vtkInternal;
  std::unique_ptr<vtkInternal> Internal;
};

#endif

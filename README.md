# ParaView Tracing plugin

This plugin uses the Log mechanism of ParaView to store information
about filters usage. Each ParaView session will append information in it,
in a csv-like format:
`User;session-id;data;info`
With `info` being a tracked event, such as a filter creation.

The log file location should be set as `PARAVIEW_STAT_FILE` environment variable.

# License
This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
